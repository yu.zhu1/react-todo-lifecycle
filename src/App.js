import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            showFlag: true,
            textOnShow:"Show",
            toDoListShow: null
        };

        this.changeShowButton = this.changeShowButton.bind(this);
        this.Refresh = this.Refresh.bind(this);
    }

    render() {
        return (
            <section className='App' >
                <div className='buttons'>
                    <button className='show' onClick={this.changeShowButton}>{this.state.textOnShow}</button>
                    <button className='refresh' onClick={this.Refresh}>Refresh</button>
                </div>
                {this.state.toDoListShow}
            </section>
        );
    }

    changeShowButton() {
        var showStatus = !this.state.showFlag;
        var showButtonResult = showStatus ? 'Show' : 'Hide';
        var showToDoList = showStatus ? null : <TodoList/>;
        this.setState({
            textOnShow: showButtonResult,
            showFlag: showStatus,
            toDoListShow: showToDoList
        })

    }

    Refresh() {
        window.location.reload();
    }

}

export default App;