import React, {Component} from 'react';
import './todolist.less';
import Add from './Add.js';

class TodoList extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      showAddList: [],
      listId: 1
    };
    this.showAddLists = this.showAddLists.bind(this);
  }

  render() {
    return (
        <section className='addList'>
          <div className='toDoList'>
            <button className='add' onClick={this.showAddLists}>Add</button>
          </div>
          {this.state.showAddList.map((item) => item)}
        </section>
  );
  }
  componentDidMount() {
    console.log('Did mount')
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('Update')
  }

  componentWillUnmount() {
    console.log('Will unmount')
  }

  showAddLists() {
    var showLists =  this.state.showAddList;
    var updateListId = this.state.listId + 1;
    showLists.push(<Add id = {this.state.listId} key = {this.state.listId}/>);
    this.setState({
      showAddList: showLists,
      listId: updateListId
    })
  }
}

export default TodoList;

